import React from 'react';
// import PaypalExpressBtn from 'react-paypal-express-checkout';

export default function CheckoutButton()  {
  
        const onSuccess = (payment) => {
            		console.log("The payment was succeeded!", payment);
                    this.props.tranSuccess(payment)
        }
 
        const onCancel = (data) => {
            console.log('The payment was cancelled!', data);
        }
 
        const onError = (err) => {
            console.log("Error!", err);
        }
 
        let currency = 'PHP'; 
        let total = this.props.total; 

        return (
            <cartbutton 
            currency={currency} 
            total={total} 
            onError={onError} 
            onSuccess={onSuccess} 
            onCancel={onCancel}
           />
        );
    }
