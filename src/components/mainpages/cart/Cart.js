import React, {useContext, useState, useEffect} from 'react'
import {GlobalState} from '../../../GlobalState'
import axios from 'axios'
// import CheckoutButton from './CheckoutButton'
import { Button } from 'react-bootstrap'

function Cart() {
    const state = useContext(GlobalState)
    const [cart, setCart] = state.userAPI.cart
    const [token] = state.token
    const [total, setTotal] = useState(0)

    useEffect(() =>{
        const getTotal = () =>{
            const total = cart.reduce((prev, item) => {
                return prev + (item.price * item.quantity)
            },0)

            setTotal(total)
        }

        getTotal()

    },[cart])

    const addToCart = async (cart) =>{
        await axios.patch('/user/addcart', {cart}, {
            headers: {Authorization: token}
        })
    }


    const increment = (id) =>{
        cart.forEach(item => {
            if(item._id === id){
                item.quantity += 1
            }
        })

        setCart([...cart])
        addToCart(cart)
    }

    const decrement = (id) =>{
        cart.forEach(item => {
            if(item._id === id){
                item.quantity === 1 ? item.quantity = 1 : item.quantity -= 1
            }
        })

        setCart([...cart])
        addToCart(cart)
    }
    // Swal.fire({
    //     title: 'Successfully enrolled!',
    //     icon: 'success',
    //     text: 'You have successfully enrolled for this course.'
    // })
    const removeProduct = id =>{
        if(window.confirm("Do you want to delete this product?")){
            cart.forEach((item, index) => {
                if(item._id === id){
                    cart.splice(index, 1)
                }
            })

            setCart([...cart])
            addToCart(cart)
        }
    }

    // const checkoutProduct = async(cart) => {
    //         await axios.push('/history/order', {cart}, {
    //             headers: {Authorization: token}
    //         })
    //     }


    // const tranSuccess = async(payment) => {
    //     const {paymentID, address} = payment;

    //     await axios.post('/api/payment', {cart, paymentID, address}, {
    //         headers: {Authorization: token}
    //     })

    //     setCart([])
    //     addToCart([])
    //     alert("You have successfully placed an order.")
    // }


    if(cart.length === 0) 
        return <h2 style={{textAlign: "center", fontSize: "5rem"}}>Cart Empty</h2> 

    return (
        <div>
            {
                cart.map(product => (
                    <div className="detail cart" key={product._id}>
                        <img src={product.images.url} alt="" />

                        <div className="box-detail">
                            <h2>{product.title}</h2>

                            <h3>₱ {product.price * product.quantity}</h3>
                            <p>{product.description}</p>
                            <p>{product.content}</p>

                            <div className="amount">
                                <Button variant="success" onClick={() => decrement(product._id)}> - </Button>
                                <span>{product.quantity}</span>
                                <Button variant="success" onClick={() => increment(product._id)}> + </Button>
                            </div>
                            
                            <div className="delete" 
                            onClick={() => removeProduct(product._id)}>
                                Remove
                            </div>

                        </div>
                    </div>
                ))
            }
                {/* <CheckoutButton
                total={total}
                tranSuccess={tranSuccess} /> */}

            <div className="total">
                <h3>Total: ₱ {total}</h3>
                {/* <Button variant="primary" type="submit" onSubmit={() => (checkoutProduct)}> Check Out </Button> */}
                {/* <CheckoutButton
                total={total}
                tranSuccess={tranSuccess}/>  */}

                
            </div>
        </div>
    )
}

export default Cart
