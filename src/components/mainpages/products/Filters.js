import React, {useContext} from 'react'
import {GlobalState} from '../../../GlobalState'

function Filters() {
    const state = useContext(GlobalState)
    const [sort, setSort] = state.productsAPI.sort
    const [search, setSearch] = state.productsAPI.search


    return (
        <div className="filter_menu">

            <input type="text" value={search} placeholder="Search for a Product here."
            onChange={e => setSearch(e.target.value.toLowerCase())} />
        </div>
    )
}

export default Filters
